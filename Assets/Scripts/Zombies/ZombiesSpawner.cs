using System.Collections;
using UnityEngine;
using PlantsVsZombies;

public class ZombiesSpawner : MonoBehaviour
{
    [SerializeField]
    private int[] zombiesInRound;

    [SerializeField]
    private float timeBetweenRounds;

    [SerializeField]
    private GameObject[] zombies;
    [SerializeField]
    private Transform[] spawnPoints;

    [Space, SerializeField]
    private float timeToSpawnZombie;

    private int round;
    private int deadZombies;
    private int aliveZombies;

    private int Round => round >= zombiesInRound.Length ? zombiesInRound.Length - 1 : round;

    private void Awake()
    {
        round = 0;
        deadZombies = 0;
    }

    public void StartSpawningZombies()
    {
        StartCoroutine(SpawnZombies());
    }

    private Vector3 GetSpawnPosition()
    {
        return spawnPoints[Random.Range(0, spawnPoints.Length)].position;
    }

    private IEnumerator SpawnZombies()
    {
        aliveZombies = 0;

        while (!GameManager.Instance.isGameOver && aliveZombies < zombiesInRound[Round])
        {
            yield return new WaitForSeconds(timeToSpawnZombie);

            var instance = Instantiate(zombies[Random.Range(0, zombies.Length)], GetSpawnPosition(), Quaternion.identity);
            Zombie zombie = instance.GetComponent<Zombie>();
            zombie.SetSpeed(Round);
            zombie.OnDied += ZombieDied;
            aliveZombies++;
        }

        yield return null;
    }

    private void ZombieDied()
    {
        deadZombies++;

        if (deadZombies >= zombiesInRound[Round])
        {
            deadZombies = 0;
            round++;

            GameManager.Instance.RoundFinished();
        }
    }
}
