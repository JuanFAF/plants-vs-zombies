using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlantsVsZombies 
{
    public class ZombieSiamese : Zombie
    {
        private const float SPEED_MULTIPLIER = 3f;

        private float maxLife;
        private bool speedBoosted;

        private void Start()
        {
            maxLife = life;
            speedBoosted = false;
        }

        public override void DoDamage(float damage)
        {
            if (life <= maxLife / 2f && speedBoosted == false) 
            {
                speedBoosted = true;
                speed *= SPEED_MULTIPLIER;
            }

            base.DoDamage(damage);
        }
    }
}
