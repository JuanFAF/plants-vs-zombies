using UnityEngine;
using System;

namespace PlantsVsZombies
{
    public class Zombie : Damageable
    {
        private const float AUGMENT_PERCENTAGE = 0.15f;

        [SerializeField]
        protected float speed;
        [SerializeField]
        protected float damage;

        private float initialSpeed;

        public event Action OnDied;

        private void Awake()
        {
            initialSpeed = speed;
        }

        private void Update()
        {
            transform.position += Vector3.left * speed * UnityEngine.Time.deltaTime;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.CompareTag("Pea")) 
            {
                Pea pea = collision.gameObject.GetComponent<Pea>();
                DoDamage(pea.Damage);
                Destroy(pea.gameObject);
            }

            Damageable plant = collision.gameObject.GetComponent<Plant>();
            if (plant != null) 
            {
                plant.DoDamage(damage);
            }
        }

        protected override void Die()
        {
            OnDied?.Invoke();

            base.Die();
        }

        public void SetSpeed(int round) 
        {
            speed = initialSpeed + round * AUGMENT_PERCENTAGE;
        }
    }
}