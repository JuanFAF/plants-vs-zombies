using System.Collections;
using UnityEngine;

namespace PlantsVsZombies
{
    public class Pea : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float damage;
        [Space, SerializeField, Range(0, 10)]
        private float releasePeaTime = 10;

        public float Damage => damage;

        private void Start()
        {
            StartCoroutine(ReleasePea());
        }

        private void Update()
        {
            transform.position += Vector3.right * speed * UnityEngine.Time.deltaTime;
        }

        private IEnumerator ReleasePea()
        {
            yield return new WaitForSeconds(releasePeaTime);

            Destroy(gameObject);
        }
    }
}