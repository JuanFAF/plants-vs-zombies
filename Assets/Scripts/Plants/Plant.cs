using UnityEngine;

public abstract class Plant : Damageable
{
    // ...

    private void Start()
    {
        StartShooting();
    }

    public abstract void StartShooting();
}
