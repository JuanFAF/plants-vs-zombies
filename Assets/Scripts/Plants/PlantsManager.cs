using UnityEngine;
using System.Collections.Generic;

public class PlantsManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] plantPrefab;

    private List<Plant> plants = new List<Plant>();

    private int plantIndex;

    public void OnLeanFingerTap(Vector3 worldPosition)
    {
        Transform nearestGrid = worldPosition.GetClosest(GameManager.Instance.GetGrid());

        if (nearestGrid.childCount > 0)
            return;
        else 
        {
            var plant = Instantiate(plantPrefab[plantIndex], nearestGrid).GetComponent<Plant>();
            plants.Add(plant);
        }
    }

    public void SetPlantIndex(int plantIndex)
    {
        this.plantIndex = plantIndex;
    }

    public void StartShooting() 
    {
        foreach (var plant in plants) 
        {
            if(plant != null) plant.StartShooting();
        }
    }
}
