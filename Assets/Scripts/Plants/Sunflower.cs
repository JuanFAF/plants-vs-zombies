using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sunflower : Plant
{
    [SerializeField]
    private float timeToSpawnSun;
    [SerializeField]
    private int sunAmount;
    [SerializeField]
    private GameObject sun;

    private IEnumerator SpawnSun()
    {
        while (!GameManager.Instance.isGameOver)
        {
            yield return new WaitForSeconds(timeToSpawnSun);

            Instantiate(sun, gameObject.transform.position, Quaternion.identity);

            GameManager.Instance.IncreaseSunScore(sunAmount);
        }

        yield return null;
    }

    public override void StartShooting()
    {
        StopAllCoroutines();

        StartCoroutine(SpawnSun());
    }
}
