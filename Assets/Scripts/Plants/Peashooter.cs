using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peashooter : Plant
{
    [SerializeField]
    protected float timeToShoot;
    [SerializeField]
    protected Transform peaSpawnPosition;
    [SerializeField]
    protected GameObject pea;

    virtual protected IEnumerator Shoot()
    {
        while (!GameManager.Instance.isGameOver)
        {
            yield return new WaitForSeconds(timeToShoot);

            Instantiate(pea, peaSpawnPosition.position, Quaternion.identity);
        }

        yield return null;
    }

    public override void StartShooting()
    {
        StopAllCoroutines();

        StartCoroutine(Shoot());
    }
}
