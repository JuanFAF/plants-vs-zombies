using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPeashooter : Peashooter
{
    [SerializeField]
    protected int peasBurst;
    [SerializeField]
    protected float timeBetweenPeas;

    override protected IEnumerator Shoot() 
    {
        while (!GameManager.Instance.isGameOver)
        {
            yield return new WaitForSeconds(timeToShoot);

            for(int i = 0; i < peasBurst; i++) 
            {
                Instantiate(pea, peaSpawnPosition.position, Quaternion.identity);
                yield return new WaitForSeconds(timeBetweenPeas);
            }
        }

        yield return null;
    }
}
