using UnityEngine;

public static class Utilities
{
    public static Transform GetClosest(this Vector3 position, Transform[] targets)
    {
        Transform closestTransform = null;

        float minDistance = Mathf.Infinity;

        foreach (Transform target in targets)
        {
            float distance = Vector3.Distance(target.position, position);

            if (distance < minDistance)
            {
                closestTransform = target;
                minDistance = distance;
            }
        }

        return closestTransform;
    }
}
