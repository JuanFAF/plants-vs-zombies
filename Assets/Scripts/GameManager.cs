using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI scoreText;

    [Space, SerializeField]
    private Transform[] grid;

    [Space, SerializeField]
    private UnityEvent onStart;
    [SerializeField]
    private UnityEvent onGameOver;

    public static GameManager Instance { get; private set; }

    public bool isGameOver { get; private set; }

    public int sunScore { get; private set; }

    private void Awake()
    {
        if (Instance != null)
            DestroyImmediate(gameObject);
        else
            Instance = this;
    }

    public void OnStart()
    {
        isGameOver = false;

        onStart?.Invoke();
    }

    public void OnGameOver()
    {
        isGameOver = true;

        onGameOver?.Invoke();
    }

    public Transform[] GetGrid()
    {
        return grid;
    }

    public void IncreaseSunScore(int amount)
    {
        sunScore += amount;

        if (scoreText != null)
            scoreText.text = sunScore.ToString();
    }

    public void RoundFinished() 
    {
        OnGameOver();

        Invoke(nameof(OnStart), 5f);
    }
}
